[![npm version](https://img.shields.io/npm/v/kelpo-lottery-wheel.svg)](https://www.npmjs.com/package/kelpo-lottery-wheel)
[![Greenkeeper badge](https://badges.greenkeeper.io/fralonra/lottery-wheel.svg)](https://greenkeeper.io/)

# lottery-wheel

A library helps you performing a wheel for lottery game. Using [anime.js](https://github.com/juliangarnier/anime/) underlying.

# Usage

```bash
npm install kelpo-lottery-wheel
```

Or download the latest [release](https://gitlab.com/kelpo123/kelpo-lottery-wheel.git).

Then link `lottery-wheel.min.js` or `lottery-wheel.js` in your HTML.

```html
<script src="/path/to/lottery-wheel.min.js"></script>
```

ESM is supported as well.

```js
import Wheel from 'kelpo-lottery-wheel';
```

Suppose you have an element whose id is 'wheel' in your html file.

```html
<svg id="wheel"></svg>
```

Then you can do the following to create a wheel:

```javascript
new Wheel({
  el: document.getElementById('wheel'),

  data: [
    {
      text: 'OVO 50.000',
      color: '#f7c600',
      chance: 30,
    },
    {
      text: 'Lamborgini Ourus',
      color: '#a4cd00',
      chance: 1,
    },
    {
      text: 'OVO 100.000',
      color: '#42a500',
      chance: 19,
    },
    {
      text: 'ToteBag Kelpo',
      color: '#2b9b85',
      chance: 10,
    },
    {
      text: 'OVO 150.000',
      color: '#01a5ce',
      chance: 10,
    },
    {
      text: 'Kaos Kelpo 21',
      color: '#3536c1',
      chance: 7,
    },
    {
      text: 'OVO 200.000',
      color: '#582b9a',
      chance: 7,
    },
    {
      text: 'Mug Kelpo 21',
      color: '#f70063',
      chance: 5,
    },
    {
      text: 'Ovo 500.000',
      color: '#cd0000',
      chance: 5,
    },
    {
      text: 'Sticker Kelpo 21',
      color: '#f76300',
      chance: 6,
    },
  ],
  radius: 300,
  duration: 15000,
  onButtonHover(anime, button) {
    anime({
      targets: button.node,
      scale: 1.2,
      duration: 500,
    });
  },
  onSuccess(data) {
    alert(data.text);
  },
  color: {
    border: '#212121',
    line: '#212121',
    button: '#212121',
  },
  buttonText: 'Mulai',
  buttonWidth: 80,
});
```

# API

## Methods

### constructor(option)

More for `option`, see [below](#options).

### draw()

To manually render the wheel when the `draw` property is set to false.

```javascript
const wheel = new Wheel({
  el: document.getElementById('wheel'),
  data: ['Beijing', 'London', 'New York', 'Tokyo'],
  draw: false,
});
setTimeout(() => {
  wheel.draw();
}, 2000);
```

## Options

| Property       | Description                                                                                                                           | Type     | Default         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------- | -------- | --------------- |
| el             | The element where the wheel mounted. [Details](#el).                                                                                  | Object   | -               |
| data           | An array of prizes. [Details](#data).                                                                                                 | Array    | -               |
| pos            | The top-left corner of the wheel related to its parent element (the `el` element).                                                    | Array    | [0, 0]          |
| radius         | The radius of the wheel in `px`.                                                                                                      | Number   | 100             |
| buttonText     | The text on the button.                                                                                                               | String   | 'Draw'          |
| fontSize       | The size of text for prizes.                                                                                                          | Number   | (auto generate) |
| buttonWidth    | The width of the button in `px`.                                                                                                      | Number   | 50              |
| buttonFontSize | The size of text on the button.                                                                                                       | Number   | (auto generate) |
| limit          | The maxium times the wheel can be run.                                                                                                | Number   | 0 (unlimited)   |
| duration       | How long will the animation last in millseconds.                                                                                      | Number   | 5000            |
| turn           | The minimum amount of circles the wheel will turn during the animation.                                                               | Number   | 4               |
| draw           | If true, the wheel will be rendered immediately the instance created. Otherwise, you should call [draw](#draw) to manually render it. | Boolean  | true            |
| clockwise      | If true, the rotation movement will be clockwise. Otherwise, it will be counter-clockwise.                                            | Boolean  | true            |
| theme          | The color preset to be used. [Details](#themes).                                                                                      | String   | 'default'       |
| image          | Allow you to render the wheel using image resources. See [image](#image).                                                             | Object   | -               |
| color          | An object used to override the color in the current theme. See [themes](#themes)                                                      | Object   | -               |
| onSuccess      | The callback function called when a prize is drawn successfully. [Details](#onsuccess).                                               | Function | -               |
| onFail         | The callback function called when trying to draw prize while has already drawn `limit` times. [Details](#onfail).                     | Function | -               |
| onButtonHover  | The function called when the mouse moves over the button. [Details](#onbuttonhover)                                                   | Function | -               |

### example

The simplest way is to implementation:

```javascript
<svg id="wheel"></svg>
```

![](https://gitlab.com/kelpo123/kelpo-lottery-wheel/-/raw/master/docs/images/data.png)
